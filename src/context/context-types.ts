export interface ContactContextState {
  contacts: any;
  loading: boolean;
  error: null;
}

export interface ContactContextValue {
  state: any;
  dispatch: any;
}

export interface ContactContextAction {
  type: any;
  payload: any;
}

export interface ReducerProps {
  state: any;
  action: any;
}

import React, { useReducer, createContext } from "react";
import {
  ContactContextState,
  ContactContextValue,
  ContactContextAction,
} from "./context-types";

const initialState: ContactContextState = {
  contacts: [
    {
      id: "098",
      name: "Diana Prince",
      email: "diana@us.army.mil",
    },
    {
      id: "099",
      name: "Bruce Wayne",
      email: "bruce@batmail.com",
    },
    {
      id: "100",
      name: "Clark Kent",
      email: "clark@metropolitan.com",
    },
  ],
  loading: false,
  error: null,
};

export const ContactContext = createContext<ContactContextValue>({
  state: initialState,
  dispatch: () => {},
});

const reducer: any = (
  state: ContactContextState,
  action: ContactContextAction
) => {
  switch (action.type) {
    case "ADD_CONTACT":
      return {
        contacts: [...state.contacts, action.payload],
      };
    case "DEL_CONTACT":
      return {
        contacts: state.contacts.filter(
          (contact: any) => contact.id !== action.payload
        ),
      };
    case "START":
      return {
        loading: true,
      };
    case "COMPLETE":
      return {
        loading: false,
      };
    default:
      throw new Error();
  }
};

export const ContactContextProvider = (props: any) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <ContactContext.Provider value={{ state, dispatch }}>
      {props.children}
    </ContactContext.Provider>
  );
};
